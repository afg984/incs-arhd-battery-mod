set -e
rm build/ dist/ -rf


mkdir -p resource/
wget --continue http://xda2.androidrevolution.nl/db_mirror/ROMs/HTC_Incredible_S/Android_Revolution_HD-Incredible_S_6.0.zip --output-document=resource/Android_Revolution_HD-Incredible_S_6.0.zip


mkdir build/
unzip -j resource/Android_Revolution_HD-Incredible_S_6.0.zip system/app/SystemUI.apk system/framework/framework-res.apk system/framework/com.htc.resources.apk -d build/


apktool install-framework build/framework-res.apk
apktool install-framework build/com.htc.resources.apk

apktool decode build/framework-res.apk build/framework-res/
apktool decode build/SystemUI.apk build/SystemUI/


rm build/framework-res/res/drawable/stat_sys_battery{,_charge}.xml
rm build/framework-res/res/drawable-hdpi/stat_sys_battery_*.png
cp resource/drawable/* build/framework-res/res/drawable/
cp resource/drawable-hdpi/* build/framework-res/res/drawable-hdpi/

rm build/SystemUI/smali/com/android/systemui/statusbar/phone/PhoneStatusBarPolicy.smali
cp resource/PhoneStatusBarPolicy.smali build/SystemUI/smali/com/android/systemui/statusbar/phone/


apktool build build/framework-res/
apktool build build/SystemUI/

cp resource/flashable_skel.zip build/flashable_skel.zip

cd build/
mkdir -p system/{app,framework}/
cp framework-res/dist/framework-res.apk system/framework/
cp SystemUI/dist/SystemUI.apk system/app/
zip -u flashable_skel.zip system/*/*
cd ..


mkdir dist/
cp build/flashable_skel.zip dist/BatteryMod_ARHD_IncS_6.0-rev1.zip


echo Done!
md5sum dist/*

